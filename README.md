## Intoduction

This is a OPEN ZEPPELIN GSN  VUE BOX template
(modified from truffle vue box template https://github.com/standup75/vue-box) 
This project is in development to run on the OpenZeppelin Gas Station Network.
References:
- https://docs.openzeppelin.com/sdk/2.6/
- https://docs.openzeppelin.com/sdk/2.6/gsn-dapp
- https://docs.openzeppelin.com/sdk/2.6/writing-contracts
- https://docs.openzeppelin.com/sdk/2.6/testing

## Requirements

Prerequisites: A ssh terminal, Node.js 10.16.3, git, yarn and npm installed.
Tested on Ubuntu 16.04 LTS Linux OS.


1. Install Truffle and Ganache-Cli test blockchain globally.
    ```javascript
    yarn add --global @openzeppelin/cli ganache-cli
    ```

## Installation

2. Download the box. This also takes care of installing the necessary dependencies.
   The box comes with everything you need to start using smart contracts from a vue app.
   From a bash terminal in projects directory: 
    
    ```javascript  
    git @gitlab.com:chevronx/vue-box-gsn.git
    ```
    This will create a directory vue-box-gsn. Then initialize openzeppelin environnement:

    ```javascript
    cd vue-box-gsn
    oz init
    ```

3. Run a test blockchain with ganache. See docs for complete overview of -port and other options. https://github.com/trufflesuite/ganache-cli/blob/master/README.md 
 
    ```javascript
    ganache-cli --deterministic
    ```

4. OpenZeppelin 'create' command wraps the truffle configs and functions:

    ```javascript
    oz create
    ```
The 'create' command compiles and deploys your contract to a network with upgradeability features. Follow prompts: select contract and network then select which variables to initialize.


5. Deploy the GSN RelayHub and Relayer instances (modifiy host and port if yours are different)

```javascript
npx oz-gsn deploy-relay-hub --ethereumNodeURL http://localhost:8545
```
then the relayer

```javascript
npx oz-gsn run-relayer --ethereumNodeURL http://localhost:8545 
```

## Run

6. Run the local server.
    ```javascript
    // Serves the front-end on http://localhost:3000
    yarn serve
    ```

## Testing

7. Truffle can run tests written in Solidity or JavaScript against your smart contracts. Note the command varies slightly if you're in or outside of the development console.
    ```javascript
    truffle test
    ```

8. To build the application for production, use the build command. A production build will be in the build_webpack folder.
    ```javascript
    yarn build
    ```
