import { fromInjected, fromConnection, useEphemeralKey } from '@openzeppelin/network'

const PROVIDER_URL = 'http://66.70.222.15:9545' // infura eventually

export function getWeb3 () {
  let web3Context = fromInjected()

  if (webContext === 'undefined') {
    web3Context = fromConnection(PROVIDER_URL, {
      gsn: { signKey: useEphemeralKey() }
    })

    try {
      // Request account access if needed
      // await ethereum.enable(); //deprecated
      console.log('web3 connected')
      // Acccounts now exposed
      // web3Context.eth.sendTransaction({/* ... */});
    } catch (error) {
      console.log('error check web3 connection')// User denied account access...
    }

    return web3Context
  } else {
    return web3Context
  }
}

// export function updateNetwork(networkId, networkName) {}
// export function updateAccounts(accounts) {}
// export function updateConnection(connected) {}

// web3Context.on(Web3Context.NetworkIdChangedEventName, updateNetwork);
// web3Context.on(Web3Context.AccountsChangedEventName, updateAccounts);
// web3Context.on(Web3Context.ConnectionChangedEventName, updateConnection);
