// Allows us to use ES6 in our migrations and tests.
//require('babel-register')

module.exports = {
  networks: {
    development: {
      host: 'xx.xx.xx.xx',
      port: 9545,
      network_id: '*', // Match any network id
      gas: 1070000
    }
  }
}
