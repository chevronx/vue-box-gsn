pragma solidity ^0.5.x;

import "@openzeppelin/contracts-ethereum-package/contracts/GSN/GSNRecipient.sol";
import "@openzeppelin/upgrades/contracts/Initializable.sol";

contract SimpleStorage is Initializable, GSNRecipient {
  event NewData(uint Update); 
 
  uint storedData;
  address private _owner;


  function initialize(uint num) public initializer {
    GSNRecipient.initialize();
    _owner = _msgSender();
    storedData = num;
  }

  function set(uint x) public {
   storedData = x;
   emit NewData(x);
  }

  function get() public view returns (uint) {
    return storedData;
  }

  function acceptRelayedCall(
   address relay,
   address from,
   bytes calldata encodedFunction,
   uint256 transactionFee,
   uint256 gasPrice,
   uint256 gasLimit,
   uint256 nonce,
   bytes calldata approvalData,
   uint256 maxPossibleCharge
   ) external view returns (uint256, bytes memory){
   return _approveRelayedCall();
  }

  function _preRelayedCall(bytes memory context) internal returns (bytes32) {
  }

  function _postRelayedCall(bytes memory context, bool, uint256 actualCharge, bytes32) internal {
  }

}

