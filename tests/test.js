//const { BN, constants, expectEvent, shouldFail } = require('openzeppelin-test-helpers');
var assert = require('chai').assert
require("babel-register");

const SimpleStorage = artifacts.require("SimpleStorage");
contract("contract test", accounts => {
         beforeEach(async function () {
         //MyContract = await SimpleStorage.new();
         });

  	 it("sets value equal to expected value", async () => {
           let val = 88
           let instance = await SimpleStorage.deployed()
           await instance.set(val)
	   await instance.get().then((r) => {
           assert.equal(val,r.toNumber());
         })
      })   

	it("should get the same value", async () => {
	let val = 88 
        let instance = await SimpleStorage.deployed();
	instance.set(val)
	.then(() => instance.get()).then((r) => {
	assert.equal(val,r.toNumber());
	})
      });
 })

describe("Tests", function () {
it('does something', () => {
        const msg = 'new message'
       	assert.equal(msg, 'new message','ok')
        })});


